(define (domain zombie-escape)
  (:requirements :typing :fluents)
  (:types human location)
  (:predicates
    (is-actor-here ?actor -human ?loc -location)
    (is-lantern-here ?loc -location))
  (:functions
    (time-required ?actor -human)
    (time-passed))
  ; one person crosses the bridge
  (:action cross-one
   :parameters (?actor -human ?loc1 ?loc2 -location)
   :precondition (and
    (is-actor-here ?actor ?loc1)
    (is-lantern-here ?loc1))
   :effect (and
    ; actor and lantern switch sides
    (not (is-actor-here ?actor ?loc1))
    (not (is-lantern-here ?loc1))
    (is-actor-here ?actor ?loc2)
    (is-lantern-here ?loc2)
    (increase (time-passed) (time-required ?actor))))
  ; two persons cross the bridge
  (:action cross-two
   :parameters (?actor1 ?actor2 -human ?loc1 ?loc2 -location)
   :precondition (and
    (not (= ?actor1 ?actor2))
    (is-actor-here ?actor1 ?loc1)
    (is-actor-here ?actor2 ?loc1)
    (is-lantern-here ?loc1))
   :effect (and
    (not (is-actor-here ?actor1 ?loc1))
    (not (is-actor-here ?actor2 ?loc1))
    (not (is-lantern-here ?loc1))
    (is-actor-here ?actor1 ?loc2)
    (is-actor-here ?actor2 ?loc2)
    (is-lantern-here ?loc2)
    (when (>= (time-required ?actor1) (time-required ?actor2))
      (increase (time-passed) (time-required ?actor1)))
    (when (< (time-required ?actor1) (time-required ?actor2))
      (increase (time-passed) (time-required ?actor2))))))
