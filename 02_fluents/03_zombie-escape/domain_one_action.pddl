(define (domain zombie-escape)
  (:requirements :typing :fluents)
  (:types human location)
  (:predicates
    (is-actor-here ?actor -human ?loc -location)
    (is-lantern-here ?loc -location))
  (:functions
    (time-required ?actor -human)
    (time-passed))
  ; same as cross-two except that we allow actor1 and actor2 to be the same
  (:action cross
   :parameters (?actor1 ?actor2 -human ?loc1 ?loc2 -location)
   :precondition (and
    (is-actor-here ?actor1 ?loc1)
    (is-actor-here ?actor2 ?loc1)
    (is-lantern-here ?loc1))
   :effect (and
    (not (is-actor-here ?actor1 ?loc1))
    (not (is-actor-here ?actor2 ?loc1))
    (not (is-lantern-here ?loc1))
    (is-actor-here ?actor1 ?loc2)
    (is-actor-here ?actor2 ?loc2)
    (is-lantern-here ?loc2)
    (when (>= (time-required ?actor1) (time-required ?actor2))
      (increase (time-passed) (time-required ?actor1)))
    (when (< (time-required ?actor1) (time-required ?actor2))
      (increase (time-passed) (time-required ?actor2))))))
