(define (domain jug-pouring)
  (:requirements :typing :fluents)
  (:types jug)
  (:functions
    ;amount of water in a jug
    (amount ?j - jug)
    ;capacity of a jug
    (capacity ?j - jug)
    ;how many actions were already done
    (pour-counter))
  ;pour entire jug
  (:action pour-to-empty
   :parameters (?jug1 ?jug2 - jug)
   :precondition (and
    ; jugs should be different
    (not (= ?jug1 ?jug2))
    ; first jug can't be empty
    (not (= (amount ?jug1) 0))
    ; the second jug has to have enough empty space
    (> (- (capacity ?jug2) (amount ?jug2)) (amount ?jug1)))
   :effect (and
    ; add amount from the first jug to the second
    (increase (amount ?jug2) (amount ?jug1))
    ; mark first jug as empty
    (assign (amount ?jug1) 0)
    ; increase pour-counter
    (increase (pour-counter) 1)))
  ;pour only part of the jug
  (:action pour-partly
   :parameters (?jug1 ?jug2 - jug)
   :precondition (and
    ; jugs should be different
    (not (= ?jug1 ?jug2))
    ; first jug can't be empty
    (not (= (amount ?jug1) 0))
    ; second jug should have less space than water contained in the first jug
    (< (- (capacity ?jug2) (amount ?jug2)) (amount ?jug1)))
   :effect (and
    ; decrease the amount of the water in the first jug
    (decrease (amount ?jug1) (- (capacity ?jug2) (amount ?jug2)))
    ; mark the second jug as full
    (assign (amount ?jug2) (capacity ?jug2))
    ; increase pour-counter
    (increase (pour-counter) 1))))
