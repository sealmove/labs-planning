(define (problem jug-pouring-example)
  (:domain jug-pouring)
  (:objects jug-19l jug-13l jug-7l fountain - jug)
  (:init
    (= (pour-counter) 0)
    ; fountain can be full since there is no water to pour into it (all jugs are empty).
    ; This prunes the search space
    (= (capacity fountain) 39)
    (= (capacity jug-19l) 19)
    (= (capacity jug-13l) 13)
    (= (capacity jug-7l) 7)
    ; fountain must have enough water to fill in all the other jugs
    ; sum(missing water for each jug) = 19 + 13 + 7 = 39
    (= (amount fountain) 39)
    (= (amount jug-19l) 0)
    (= (amount jug-13l) 0)
    (= (amount jug-7l) 0))
  (:goal (and (= (amount jug-19l) 10)
    (= (amount jug-13l) 10)
    (= (amount jug-7l) 0)))
  (:metric minimize (pour-counter)))
