(define (problem three)
  (:domain blocksworld)
  (:objects a b c)
  (:init
   (on c a)
   (ontable a)
   (ontable b)
   (clear b)
   (clear c)
   (handempty))
  (:goal (and
   (on b c)
   (on a b))))
