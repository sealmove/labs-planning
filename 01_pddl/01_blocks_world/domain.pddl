(define (domain blocksworld) 
  (:requirements :strips) ; STRIPS required
  (:predicates
    (on ?x ?y)    ; block ?x is on block ?y
    (ontable ?x)  ; block ?x on the table
    (clear ?x)    ; no block is on block ?x
    (handempty)   ; robot’s arm is empty
    (holding ?x)) ; robot’s arm is holding ?x
  (:action pick-up-from-table ; action "pick up from the table"
   :parameters (?block)
   :precondition (and
    (ontable ?block)
    (clear ?block)
    (handempty))
   :effect (and
    (not (ontable ?block))
    (not (clear ?block))
    (not (handempty))
    (holding ?block)))
  (:action put-on-table ; action "put on the table"
   :parameters (?block)
   :precondition (holding ?block)
   :effect (and
    (ontable ?block)
    (clear ?block)
    (handempty)
    (not (holding ?block))))
  (:action put-on-block ; action "put block A on block B"
   :parameters (?blockA ?blockB)
   :precondition (and
    (clear ?blockB)
    (holding ?blockA))
   :effect (and
    (on ?blockA ?blockB)
    (not (clear ?blockB))
    (clear ?blockA)
    (handempty)
    (not (holding ?blockA))))
  (:action take-off-block ; action "take block A off block B"
   :parameters (?blockA ?blockB)
   :precondition (and
    (on ?blockA ?blockB)
    (clear ?blockA)
    (handempty))
   :effect (and
    (not (on ?blockA ?blockB))
    (clear ?blockB)
    (not (clear ?blockA))
    (not (handempty))
    (holding ?blockA))))
