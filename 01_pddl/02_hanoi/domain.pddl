; a pile is modelled as an immutable disc which is bigger than any other disc
(define (domain hanoi)
  (:requirements :strips)
  (:predicates
   (smaller ?x ?y)
   (on ?x ?y)
   (clear ?x))
  (:action move
   :parameters (?disc ?from ?to)
   :precondition (and
    (smaller ?disc ?to)
    (on ?disc ?from)
    (clear ?disc)
    (clear ?to))
   :effect (and
    (clear ?from)
    (not (clear ?to))
    (on ?disc ?to)
    (not (on ?disc ?from)))))
